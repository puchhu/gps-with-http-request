#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <ESP8266HTTPClient.h>

static const int RXPin = 4, TXPin = 5;       // GPIO 4=D2(conneect Tx of GPS) and GPIO 5=D1(Connect Rx of GPS)
static const uint32_t GPSBaud = 9600;       //if Baud rate 9600 didn't work in your case then use 4800

TinyGPSPlus gps;                          // The TinyGPS++ object
WidgetMap myMap(V0);                     // V0 for virtual pin of Map Widget

SoftwareSerial ss(RXPin, TXPin);         // The serial connection to the GPS device

BlynkTimer timer;

float spd;                           //Variable  to store the speed
float sats;                         //Variable to store no. of satellites response
String bearing;                    //Variable to store orientation or direction of GPS
HTTPClient http;
String response;

char auth[] = "2tdyr93uhvXm6ohJ3glpEnP000XFzHJW";            
char ssid[] = "Raj";                                       
char pass[] = "raj@00321";                                      
//unsigned int move_index;         // moving index, to be used later
unsigned int move_index = 1;       // fixed location for now
float lat1;
float long1;
String lat2;
String long2;
void setup(){
  Serial.begin(9600);
//  Serial.println();
  ss.begin(GPSBaud);
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(5000L, checkGPS); // every 5s check if GPS is connected, only really needs to be done once
}

void checkGPS(){
  if (gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
      Blynk.virtualWrite(V4, "GPS ERROR");                 // Value Display widget  on V4 if GPS not detected
  }
}

void loop(){
  Blynk.run();
  timer.run();
  while (ss.available() > 0){
    if (gps.encode(ss.read()))
      displayInfo();
      //postData();
  }
  
}

void displayInfo(){ 
  if (gps.location.isValid() ){
    float latitude = (gps.location.lat());          //Storing the Lat. and Lon. 
    float longitude = (gps.location.lng()); 
    lat2=String(latitude,6);
    long2=String(longitude,6);
    Serial.print("LAT:  ");
    Serial.println(lat2);                  // float to x decimal places
    Serial.print("LONG: ");
    Serial.println(long2);
    postData();
    Blynk.virtualWrite(V1, lat2);   
    Blynk.virtualWrite(V2, long2);  
    myMap.location(move_index, lat2, long2, "GPS_Location");
    spd = gps.speed.kmph();                     //get speed
       Blynk.virtualWrite(V3, spd);
       
       sats = gps.satellites.value();         //get number of satellites
       Blynk.virtualWrite(V4, sats);

       bearing = TinyGPSPlus::cardinal(gps.course.value());     // get the direction
       Blynk.virtualWrite(V5, bearing); 
                      
  }
}

void postData(){
    Serial.println("..................");
    delay(1000);
    http.begin("http://carsmart.herokuapp.com/transaction/capture_gps?longitude="+long2+"&latitude="+lat2+"&device_id="+1);   //Specify request destination
    http.addHeader("Content-Type", "text/plain");  //Specify content-type header
    int httpCode = http.POST("dhondu");   //Send the request
    String payload = http.getString();   //Get the response payload
    response = payload;
    Serial.println(response);
  http.end();
  Serial.println("------------------");
}
